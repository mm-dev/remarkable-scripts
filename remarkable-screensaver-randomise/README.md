# Randomise Screensaver Images for reMarkable Tablet

Periodically change the images the reMarkable uses as screensavers, by choosing randomly from a folder of JPEGs.

- A script overwrites `suspended.png` and `poweroff.png` with links to the random images
- A timer calls the script repeatedly, at an interval which can easily be changed (see below).


## Installation

Via SSH/SCP (or any method you prefer), on the reMarkable:

0. Create the directory `/home/root/screensaver-random-images` and fill it with images
    - Images should be JPEGs, **named with `.jpg` file extensions**
    - For best image quality (to match the rM resolution) the **image dimensions should be 1404x1872**
0. Copy `screensaver-randomise-symlinks.sh` into `/home/root`
0. Copy both systemd unit files to `/etc/systemd/system`
    - Optional: Edit `screensaver-randomise.timer` to set how frequently the images will be randomised (default is every 2 minutes)
0. Install and start the service:
    - `systemctl enable screensaver-randomise.timer`
    - `systemctl start screensaver-randomise.timer`

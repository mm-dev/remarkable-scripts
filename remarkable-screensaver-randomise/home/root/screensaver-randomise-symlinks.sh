#!/opt/bin/bash

# reMarkable uses `suspended.png` and `poweroff.png` as screensaver images
# This script replaces those files, with symbolic links to files randomly
# chosen from a folder full of images

# This script is intended to be called periodically via eg a systemd timer

# Absolute path to directory containing JPEG images
CHOICE_DIR=/home/root/screensaver-random-images

# Array containing absolute paths to files which will be replaced
# Each time this script is called, all of these files will be replaced
REPLACE_FILE_AR=(
  "/usr/share/remarkable/suspended.png"
  "/usr/share/remarkable/poweroff.png"
)

for LINK_PATH in "${REPLACE_FILE_AR[@]}"; do
    RANDOM_FILE=$(find "$CHOICE_DIR" -maxdepth 1 -iname "*.jpg" | shuf -n1)
    rm "$LINK_PATH"
    ln -s "$RANDOM_FILE" "$LINK_PATH"
done
